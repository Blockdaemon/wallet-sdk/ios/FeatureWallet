// Copyright © Blockdaemon All rights reserved.

import Combine
import CryptoKit
import DIKit
import Extensions
import Foundation
import FeatureWalletDomain

final class CryptoAssetAddressRepository: CryptoAssetAddressRepositoryAPI {
    
    private let client: CryptoAssetAddressClientAPI
    
    init(client: CryptoAssetAddressClientAPI = resolve()) {
        self.client = client
    }
    
    func fetchCryptoAssetAddressForProtocolId(
        _ protocolId: String,
        keyId: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressRepositoryError> {
        client
            .fetchCryptoAssetAddressForProtocolId(protocolId, userId: userId)
            .mapError(CryptoAssetAddressRepositoryError.network)
            .flatMap { addresses -> AnyPublisher<String, CryptoAssetAddressRepositoryError> in
                guard let address = addresses.first else {
                    return .failure(CryptoAssetAddressRepositoryError.noAddressForProtocolId(protocolId))
                }
                return .just(address.address)
            }
            
            .eraseToAnyPublisher()
    }

    
    func createCryptoAddressForBlockchain(
        _ blockchain: String,
        network: String,
        keyId: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressRepositoryError> {
        client
            .createCryptoAddressForBlockchain(
                blockchain,
                network: network,
                keyId: keyId,
                userId: userId
            )
            .map(\.address)
            .mapError(CryptoAssetAddressRepositoryError.network)
            .eraseToAnyPublisher()
    }
}
