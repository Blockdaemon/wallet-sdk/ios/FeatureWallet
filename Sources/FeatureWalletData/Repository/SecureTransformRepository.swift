// Copyright © Blockdaemon All rights reserved.

import Combine
import CryptoKit
import DIKit
import Foundation
import ThresholdSecurityDomain
import FeatureWalletDomain
import ToolKit

final class SecureTransformRepository: SecureTransformRepositoryAPI {
    
    var temporaryPassword: AnyPublisher<Data, SecureTransformError> {
        repository
            .temporaryPassword
            .mapError(SecureTransformError.privateKeyRepositoryError)
            .eraseToAnyPublisher()
    }
    
    private let repository: PrivateKeyRepositoryAPI
    
    init(repository: PrivateKeyRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    func encryptWallet(
        _ wallet: Wallet,
        userId: String
    ) -> AnyPublisher<Data, SecureTransformError> {
        repository
            .createOrFetchEncryptionKeyDataForUserId(userId)
            .tryMap { data in
                let key = try SymmetricKey(rawRepresentation: data)
                let encoded = try JSONEncoder().encode(wallet)
                do {
                    let encrypted = try AES.GCM.seal(encoded, using: key).combined
                    if let encrypted = encrypted {
                        return encrypted
                    } else {
                        throw SecureTransformError.encryptionFailed
                    }
                } catch {
                    Logger.shared.error(error)
                    throw SecureTransformError.encryptionFailed
                }
            }
            .replaceError(with: .encryptionFailed)
            .eraseToAnyPublisher()
    }
    
    func decryptWalletPayload(
        _ payload: Data,
        userId: String
    ) -> AnyPublisher<Wallet, SecureTransformError> {
        repository
            .fetchEncryptionKeyDataForUserId(userId)
            .mapError(SecureTransformError.privateKeyRepositoryError)
            .tryMap { data in
                let key = try SymmetricKey(rawRepresentation: data)
                let sealedBox = try AES.GCM.SealedBox(combined: data)
                let walletData = try AES.GCM.open(sealedBox, using: key)
                return try JSONDecoder().decode(Wallet.self, from: walletData)
            }
            .replaceError(with: SecureTransformError.decryptionFailed)
    }
    
    func encryptWalletWithTemporaryPassword(
        _ wallet: Wallet,
        password: Data
    ) -> AnyPublisher<Data, SecureTransformError> {
        Future<Data, SecureTransformError> { promise in
            do {
                let key = SymmetricKey(data: password)
                let encoded = try JSONEncoder().encode(wallet)
                let sealedBox = try AES.GCM.seal(encoded, using: key)
                guard let combined = sealedBox.combined else {
                    promise(.failure(SecureTransformError.encryptionFailed))
                    return
                }
                promise(.success(combined))
            } catch {
                Logger.shared.error(error)
                promise(.failure(SecureTransformError.encryptionFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    func decryptWalletWithTemporaryPassword(
        _ encryptedData: Data,
        password: Data
    ) -> AnyPublisher<Wallet, SecureTransformError> {
        Future<Wallet, SecureTransformError> { promise in
            do {
                let key = SymmetricKey(data: password)
                let sealedBox = try AES.GCM.SealedBox(combined: encryptedData)
                let walletData = try AES.GCM.open(sealedBox, using: key)
                let wallet = try JSONDecoder().decode(Wallet.self, from: walletData)
                promise(.success(wallet))
            } catch {
                promise(.failure(SecureTransformError.decryptionFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
}
