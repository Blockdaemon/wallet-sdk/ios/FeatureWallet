// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors

protocol CryptoAssetAddressClientAPI {

    func createCryptoAddressForBlockchain(
        _ blockchain: String,
        network: String,
        keyId: String,
        userId: String
    ) -> AnyPublisher<CryptoAddress, NetworkError>
    
    func fetchCryptoAssetAddressForProtocolId(
        _ protocolId: String,
        userId: String
    ) -> AnyPublisher<[CryptoAddress], NetworkError>
}
