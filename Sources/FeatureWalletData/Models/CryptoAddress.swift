// Copyright © Blockdaemon All rights reserved.

struct CryptoAddress: Decodable {
    let address: String
    let blockchain: String
    let network: String
}
