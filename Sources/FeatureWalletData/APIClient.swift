// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Foundation
import NetworkKit

final class APIClient: CryptoAssetAddressClientAPI {
    
    private enum Path: String {
        
        case addresses = "addresses"
        
        static func makePathWithUserId(
            _ userId: String,
            path: Path
        ) -> [String] {
            ["waas", "v1", "application-users", userId, "wallet-operations", path.rawValue]
        }
    }
    
    private enum Parameter { 
        static let protocolId = "protocol-id"
    }
    
    private let networkAdapter: NetworkAdapterAPI
    private let requestBuilder: RequestBuilder
    
    // MARK: - Init
    
    init(
        requestBuilder: RequestBuilder = resolve(),
        networkAdapter: NetworkAdapterAPI = resolve()
    ) {
        self.requestBuilder = requestBuilder
        self.networkAdapter = networkAdapter
    }
    
    func fetchCryptoAssetAddressForProtocolId(
        _ protocolId: String,
        userId: String
    ) -> AnyPublisher<[CryptoAddress], NetworkError> {
        
        struct Response: Decodable {
            let data: [CryptoAddress]
        }
        
        let request = requestBuilder.get(
            path: Path.makePathWithUserId(userId, path: .addresses),
            parameters: [
                .init(name: Parameter.protocolId, value: protocolId)
            ],
            headers: [HttpHeaderField.accept: HttpHeaderValue.json],
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.data)
            .eraseToAnyPublisher()
    }
    
    func createCryptoAddressForBlockchain(
        _ blockchain: String,
        network: String,
        keyId: String,
        userId: String
    ) -> AnyPublisher<CryptoAddress, NetworkError> {
        struct Body: Encodable {
            let blockchain: String
            let network: String
            let keyId: String
        }
        
        let body = Body(blockchain: blockchain, network: network, keyId: keyId)
        
        let request = requestBuilder.post(
            path: Path.makePathWithUserId(userId, path: .addresses),
            body: try? JSONEncoder().encode(body),
            headers: [HttpHeaderField.accept: HttpHeaderValue.json],
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request)
            .eraseToAnyPublisher()
    }
}
