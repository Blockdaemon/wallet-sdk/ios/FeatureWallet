// Copyright © Blockdaemon All rights reserved.

import DIKit
import FeatureWalletDomain

extension DependencyContainer {
    
    // MARK: - FeatureWallet
    
    public static var featureWalletData = module {
        
        factory { APIClient() as CryptoAssetAddressClientAPI }
        
        factory { SecureTransformRepository() as SecureTransformRepositoryAPI }
        
        factory { CryptoAssetAddressRepository() as CryptoAssetAddressRepositoryAPI }
    }
}
