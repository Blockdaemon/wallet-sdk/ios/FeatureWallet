// Copyright © Blockdaemon All rights reserved.

import Combine
import CombineExtensions
import DIKit
import Extensions
import Errors

public enum CryptoAssetAddressRepositoryError: Error {
    case noAddressForProtocolId(String)
    case network(NetworkError)
}

public protocol CryptoAssetAddressRepositoryAPI {
    
    func fetchCryptoAssetAddressForProtocolId(
        _ protocolId: String,
        keyId: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressRepositoryError>
    
    func createCryptoAddressForBlockchain(
        _ blockchain: String,
        network: String,
        keyId: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressRepositoryError>
}
