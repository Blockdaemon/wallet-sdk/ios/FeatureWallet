// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

public struct ERSRecoveryData {
    public let ersLabel: String
    public let encryptionKey: Data
    public let password: String
    public let ersRecoveryKeys: [ERSRecoveryKey]
    
    public init(ersLabel: String, encryptionKey: Data, password: String, ersRecoveryKeys: [ERSRecoveryKey]) {
        self.ersLabel = ersLabel
        self.encryptionKey = encryptionKey
        self.password = password
        self.ersRecoveryKeys = ersRecoveryKeys
    }
}

public struct ERSRecoveryKey {
    public let curve: Curve
    public let recoveryData: Data
    
    public init(curve: String, recoveryData: Data) {
        self.curve = Curve(rawValue: curve)!
        self.recoveryData = recoveryData
    }
}
