// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct EncryptedWalletBackup {
    public let encryptedPayload: Data
    public let encryptionKey: Data
}
