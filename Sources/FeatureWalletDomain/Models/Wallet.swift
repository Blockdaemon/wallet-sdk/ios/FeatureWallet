// Copyright © Blockdaemon All rights reserved.

import Foundation
import CommonCryptoKit
import ThresholdSecurityDomain

public enum WalletInitializerError: Error {
    case incompleteWallet
}

public struct Wallet: Codable, Equatable {
    private var keyGens: [Curve: KeyGen]
    
    public subscript(curve: Curve) -> KeyGen? {
        get {
            return keyGens[curve]
        }
        set {
            if let newValue = newValue {
                keyGens[newValue.curve] = newValue
            } else {
                keyGens.removeValue(forKey: curve)
            }
        }
    }
    
    public init(keyGens: [Curve: KeyGen]) throws {
        guard keyGens.keys.count == Curve.allCases.count else {
            throw WalletInitializerError.incompleteWallet
        }
        
        self.keyGens = keyGens
    }
    
    public mutating func add(_ keyGen: KeyGen) {
        keyGens[keyGen.curve] = keyGen
    }
    
    public func allKeyGens() -> [KeyGen] {
        return Array(keyGens.values)
    }
}
