// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

public struct RecoveryKey {
    public let curve: Curve
    public let keyType: String
    public let data: Data
    public let chainCode: Data
}
