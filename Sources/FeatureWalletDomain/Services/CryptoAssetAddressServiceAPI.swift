// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Combine
import CombineExtensions
import DIKit
import Extensions
import ThresholdSecurityDomain
import ToolKit

public enum CryptoAssetAddressServiceError: Error {
    case repository(Error)
    case unsupportedCurveName
    case keyIdNotFound
}

public enum Network: String {
    case testnet
    case mainnet
}

public protocol CryptoAssetAddressServiceAPI {
    
    func fetchOrCreateAssetAddressForBlockchain(
        _ blockchain: String,
        network: String,
        curveName: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressServiceError>
    
    func createCryptoAddressForBlockchain(
        _ blockchain: String,
        network: String,
        curveName: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressServiceError>
}

final class CryptoAssetAddressService: CryptoAssetAddressServiceAPI {
    
    private let repository: CryptoAssetAddressRepositoryAPI
    private let keyIdDirectory: KeyIdDirectoryAPI
    
    init(repository: CryptoAssetAddressRepositoryAPI = resolve(),
         keyIdDirectory: KeyIdDirectoryAPI = resolve()) {
        self.repository = repository
        self.keyIdDirectory = keyIdDirectory
    }
    
    func fetchOrCreateAssetAddressForBlockchain(
        _ blockchain: String,
        network: String,
        curveName: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressServiceError> {
        createCryptoAddressForBlockchain(
            blockchain,
            network: network,
            curveName: curveName,
            userId: userId
        )
        .eraseToAnyPublisher()
    }
    
    func createCryptoAddressForBlockchain(
        _ blockchain: String,
        network: String,
        curveName: String,
        userId: String
    ) -> AnyPublisher<String, CryptoAssetAddressServiceError> {
        guard let curve = Curve(rawValue: curveName) else { return .failure(.unsupportedCurveName) }
        guard let keyId = keyIdDirectory[userId, curve]?.identifier else {
            return .failure(.keyIdNotFound)
        }
        return repository
            .createCryptoAddressForBlockchain(blockchain, network: network, keyId: keyId, userId: userId)
            .mapError(CryptoAssetAddressServiceError.repository)
            .eraseToAnyPublisher()
    }
}
