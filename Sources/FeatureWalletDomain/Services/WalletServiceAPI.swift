// Copyright © Blockdaemon All rights reserved.

import Combine
import CombineExtensions
import CommonCryptoKit
import DIKit
import Extensions
import ThresholdSecurityDomain
import ToolKit

public protocol WalletRestorationAPI {

    func generateERSRecoveryDataWithUserId(
        _ userId: String
    ) -> AnyPublisher<ERSRecoveryData, WalletLifecycleError>

    func generateWalletRestorationPayloadWithUserId(
        _ userId: String
    ) -> AnyPublisher<EncryptedWalletBackup, WalletLifecycleError>

    func restoreWalletWithUserId(
        _ userId: String,
        backup: Data
    ) -> AnyPublisher<Void, WalletLifecycleError>

    func restoreWalletWithUserId(
        _ userId: String,
        backup: Data,
        encryptionKey: Data
    ) -> AnyPublisher<Void, WalletLifecycleError>

    func extractRecoveryKeysFromERSRecoveryData(
        _ recoveryData: ERSRecoveryData
    ) -> AnyPublisher<[RecoveryKey], WalletLifecycleError>
}

public protocol WalletFactoryAPI {

    func doesUserIdHaveKeyIdForAllCurveTypes(
        _ userId: String
    ) -> AnyPublisher<Bool, Never>

    func createWalletPayloadWithUserId(
        _ userId: String
    ) -> AnyPublisher<Data, WalletLifecycleError>
}

public protocol WalletLifecycleAPI: WalletFactoryAPI, WalletRestorationAPI { }

public enum WalletLifecycleError: Error {
    case emptyKeyShareForCurve(Curve)
    case session(Error)
    case keygen(KeyGenRepositoryError)
    case encryption(SecureTransformError)
    case decryption(SecureTransformError)
    case wallet(Error)
    case keyDirectory(KeyIdDirectoryError)
    case export(TSMGlobalServiceError)
    case ers(RecoveryRepositoryError)
    case keyPair(ERSKeyPairRepositoryError)
}

final class WalletLifecycleService: WalletLifecycleAPI {

    private let sessionService: SessionServiceAPI = resolve()
    private let directory: KeyIdDirectoryAPI = resolve()
    private let privateKeyRepository: PrivateKeyRepositoryAPI = resolve()
    private let keygenRepositoryProvider: KeyGenRepositoryProviding = resolve()
    private let transformRepository: SecureTransformRepositoryAPI = resolve()
    private let tsmGlobalService: TSMGlobalServiceAPI = resolve()
    private let recoveryRepositoryProvider: RecoveryRepositoryProviding = resolve()
    private let ersKeyPairRepository: ERSKeyPairRepositoryAPI = resolve()

    // MARK: - WalletFactoryAPI

    func createWalletPayloadWithUserId(
        _ userId: String
    ) -> AnyPublisher<Data, WalletLifecycleError> {
        Publishers.Zip(
            createKeyGenForCurve(.ed25519, userId: userId),
            createKeyGenForCurve(.secp256k1, userId: userId)
        )
        .tryMap { (eddsa, ecdsa) in
            try Wallet(
                keyGens: [
                    .ed25519: eddsa,
                    .secp256k1: ecdsa
                ]
            )
        }
        .mapError(WalletLifecycleError.wallet)
        .flatMap { [transformRepository, directory] wallet in
            transformRepository
                .encryptWallet(wallet, userId: userId)
                .mapError(WalletLifecycleError.encryption)
                .flatMap { data in
                    directory
                        .addKeyIdsToUserId(userId, keyIds: wallet.allKeyGens().map(\.keyId))
                        .mapError(WalletLifecycleError.keyDirectory)
                        .map { data }
                }
        }
        .eraseToAnyPublisher()
    }

    func doesUserIdHaveKeyIdForAllCurveTypes(
        _ userId: String
    ) -> AnyPublisher<Bool, Never> {
        directory
            .doesUserIdHaveKeyIdForAllCurveTypes(userId)
    }


    // MARK: - WalletRestorationAPI

    func restoreWalletWithUserId(
        _ userId: String,
        backup: Data
    ) -> AnyPublisher<Void, WalletLifecycleError> {
        transformRepository
            .decryptWalletPayload(backup, userId: userId)
            .mapError(WalletLifecycleError.decryption)
            .flatMap { wallet -> AnyPublisher<[KeyId], WalletLifecycleError> in
                guard let eddsa = wallet[.ed25519]?.keyshare else {
                    return .failure(WalletLifecycleError.emptyKeyShareForCurve(.ed25519))
                }
                guard let ecdsa = wallet[.secp256k1]?.keyshare else {
                    return .failure(WalletLifecycleError.emptyKeyShareForCurve(.ed25519))
                }
                return Publishers.Zip(
                    self.restoreKeyGenForCurve(.ed25519, userId: userId, keyshare: eddsa),
                    self.restoreKeyGenForCurve(.secp256k1, userId: userId, keyshare: ecdsa)
                )
                .map { [$0.0, $0.1] }
                .map { $0.map(\.keyId) }
                .eraseToAnyPublisher()
            }
            .flatMap { [directory] keyIds in
                directory
                    .addKeyIdsToUserId(userId, keyIds: keyIds)
                    .mapError(WalletLifecycleError.keyDirectory)
            }
            .eraseToAnyPublisher()
    }

    func restoreWalletWithUserId(
        _ userId: String,
        backup: Data,
        encryptionKey: Data
    ) -> AnyPublisher<Void, WalletLifecycleError> {
        transformRepository
            .decryptWalletWithTemporaryPassword(backup, password: encryptionKey)
            .mapError(WalletLifecycleError.decryption)
            .flatMap { wallet -> AnyPublisher<[KeyId], WalletLifecycleError> in
                guard let eddsa = wallet[.ed25519]?.keyshare else {
                    return .failure(WalletLifecycleError.emptyKeyShareForCurve(.ed25519))
                }
                guard let ecdsa = wallet[.secp256k1]?.keyshare else {
                    return .failure(WalletLifecycleError.emptyKeyShareForCurve(.ed25519))
                }
                return Publishers.Zip(
                    self.restoreKeyGenForCurve(.ed25519, userId: userId, keyshare: eddsa),
                    self.restoreKeyGenForCurve(.secp256k1, userId: userId, keyshare: ecdsa)
                )
                .map { [$0.0, $0.1] }
                .map { $0.map(\.keyId) }
                .eraseToAnyPublisher()
            }
            .flatMap { [directory] keyIds in
                directory
                    .addKeyIdsToUserId(userId, keyIds: keyIds)
                    .mapError(WalletLifecycleError.keyDirectory)
            }
            .eraseToAnyPublisher()
    }

    func generateWalletRestorationPayloadWithUserId(
        _ userId: String
    ) -> AnyPublisher<EncryptedWalletBackup, WalletLifecycleError> {
        guard let secp256k1 = directory[userId, .secp256k1]?.identifier else {
            return .failure(.emptyKeyShareForCurve(.secp256k1))
        }
        guard let ed25519 = directory[userId, .ed25519]?.identifier else {
            return .failure(.emptyKeyShareForCurve(.ed25519))
        }
        return Publishers.Zip(
            keygenRepositoryProvider.make(with: .secp256k1)
                .generateBackupKeyShareForKeyId(secp256k1)
                .mapError(WalletLifecycleError.keygen),
            keygenRepositoryProvider.make(with: .ed25519)
                .generateBackupKeyShareForKeyId(ed25519)
                .mapError(WalletLifecycleError.keygen)
        )
        .tryMap { (secp256k1, ed25519) in
            try Wallet(
                keyGens: [
                    .secp256k1: secp256k1,
                    .ed25519: ed25519
                ]
            )
        }
        .mapError(WalletLifecycleError.wallet)
        .flatMap { [transformRepository, tsmGlobalService] wallet in
            transformRepository
                .temporaryPassword
                .mapError(WalletLifecycleError.encryption)
                .flatMap { temporaryPassword in
                    transformRepository
                        .encryptWalletWithTemporaryPassword(wallet, password: temporaryPassword)
                        .mapError(WalletLifecycleError.encryption)
                        .map { walletData in
                            EncryptedWalletBackup(encryptedPayload: walletData, encryptionKey: temporaryPassword)
                        }
                        .flatMap { backup in
                            tsmGlobalService
                                .nuke()
                                .mapError(WalletLifecycleError.export)
                                .map { _ in backup }
                        }
                }
                .eraseToAnyPublisher()
        }
        .eraseToAnyPublisher()
    }

    func extractRecoveryKeysFromERSRecoveryData(
        _ recoveryData: ERSRecoveryData
    ) -> AnyPublisher<[RecoveryKey], WalletLifecycleError> {
        guard let eddsa = recoveryData.ersRecoveryKeys.first(where: { $0.curve == .ed25519 }) else {
            return .failure(.ers(.unknown))
        }
        guard let ecdsa = recoveryData.ersRecoveryKeys.first(where: { $0.curve == .secp256k1 }) else {
            return .failure(.ers(.unknown))
        }
        let ersPrivateKey = ersKeyPairRepository
            .decryptPrivateKeyWithPassword(
                recoveryData.password,
                privateKey: recoveryData.encryptionKey
            )

        let secp256k1 = ersPrivateKey
            .mapError(WalletLifecycleError.keyPair)
            .flatMap { [recoveryRepositoryProvider] decryptedPrivateKey in
                recoveryRepositoryProvider
                    .make(with: .secp256k1)
                    .fetchRecoveryKeyWithRecoveryInfo(
                        ecdsa.recoveryData,
                        ersPrivateKey: decryptedPrivateKey,
                        ersLabel: recoveryData.ersLabel
                    )
                    .mapError(WalletLifecycleError.ers)
            }

        let ed25519 = ersPrivateKey
            .mapError(WalletLifecycleError.keyPair)
            .flatMap { [recoveryRepositoryProvider] decryptedPrivateKey in
                recoveryRepositoryProvider
                    .make(with: .ed25519)
                    .fetchRecoveryKeyWithRecoveryInfo(
                        eddsa.recoveryData,
                        ersPrivateKey: decryptedPrivateKey,
                        ersLabel: recoveryData.ersLabel
                    )
                    .mapError(WalletLifecycleError.ers)
            }

        return Publishers
            .Zip(
                secp256k1,
                ed25519
            )
            .map {
                [
                    .init(curve: $0.0.curve, keyType: "ecdsa", data: $0.0.privateKey, chainCode: $0.0.chainCode),
                    .init(curve: $0.1.curve, keyType: "eddsa", data: $0.1.privateKey, chainCode: $0.0.chainCode)
                ]
            }
            .eraseToAnyPublisher()
    }

    func generateERSRecoveryDataWithUserId(
        _ userId: String
    ) -> AnyPublisher<ERSRecoveryData, WalletLifecycleError> {
        let label = UUID().uuidString

        return ersKeyPairRepository
            .keyPair
            .mapError(WalletLifecycleError.keyPair)
            .flatMap { [recoveryRepositoryProvider] keyPair in

                let secp256k1 = recoveryRepositoryProvider
                    .make(with: .secp256k1)
                    .fetchRecoveryInfoForUserId(
                        userId,
                        // TSM requires UUID to be lower case
                        sessionId: UUID().uuidString.lowercased(),
                        ersLabel: label,
                        ersPublicKey: keyPair.publicKey
                    )
                    .mapError(WalletLifecycleError.ers)

                let ed25519 = recoveryRepositoryProvider
                    .make(with: .ed25519)
                    .fetchRecoveryInfoForUserId(
                        userId,
                        // TSM requires UUID to be lower case
                        sessionId: UUID().uuidString.lowercased(),
                        ersLabel: label,
                        ersPublicKey: keyPair.publicKey
                    )
                    .mapError(WalletLifecycleError.ers)

                return Publishers.Zip(secp256k1, ed25519)
                    .map { (secp256k1, ed25519) in
                        ERSRecoveryData(
                            ersLabel: label,
                            encryptionKey: keyPair.encryptedPrivateKey,
                            password: keyPair.password,
                            ersRecoveryKeys: [
                                .init(curve: Curve.secp256k1.rawValue, recoveryData: secp256k1),
                                .init(curve: Curve.ed25519.rawValue, recoveryData: ed25519)
                            ]
                        )
                    }
            }
            .eraseToAnyPublisher()
    }

    // MARK: - Private Functions

    private func generateSessionIdForUserId(
        _ userId: String,
        curve: Curve
    ) -> AnyPublisher<String, WalletLifecycleError> {
        sessionService
            .generateKeygenSessionIdForUserId(userId, curve: curve)
            .mapError(WalletLifecycleError.session)
            .eraseToAnyPublisher()
    }

    private func restoreKeyGenForCurve(
        _ curve: Curve,
        userId: String,
        keyshare: Data
    ) -> AnyPublisher<KeyGen, WalletLifecycleError> {
        sessionService
            .generateKeygenSessionIdForUserId(userId, curve: curve)
            .mapError(WalletLifecycleError.session)
            .flatMap { [keygenRepositoryProvider] sessionId in
                let repository = keygenRepositoryProvider.make(with: curve)
                return repository.restoreKeysForSessionId(sessionId, backup: keyshare)
                    .mapError(WalletLifecycleError.keygen)
            }
            .eraseToAnyPublisher()
    }

    private func createKeyGenForCurve(
        _ curve: Curve,
        userId: String
    ) -> AnyPublisher<KeyGen, WalletLifecycleError> {
        sessionService
            .generateKeygenSessionIdForUserId(userId, curve: curve)
            .mapError(WalletLifecycleError.session)
            .flatMap { [keygenRepositoryProvider] sessionId in
                let repository = keygenRepositoryProvider.make(with: curve)
                return repository.generateKeysForSessionId(
                    sessionId,
                    userId: userId
                )
                .mapError(WalletLifecycleError.keygen)
            }
            .eraseToAnyPublisher()
    }
}
