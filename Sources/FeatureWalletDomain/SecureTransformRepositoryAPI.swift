// Copyright © Blockdaemon All rights reserved.

import Foundation
import Combine
import ThresholdSecurityDomain

protocol DecryptionRepositoryAPI {
    func decryptWalletPayload(
        _ payload: Data
    ) -> AnyPublisher<Wallet, SecureTransformError>
}

protocol EncryptionRepositoryAPI {
    func encryptWallet(
        _ wallet: Wallet
    ) -> AnyPublisher<Data, SecureTransformError>
}

public enum SecureTransformError: Error {
    case encryptionFailed
    case privateKeyRepositoryError(PrivateKeyRepositoryError)
    case decryptionFailed
    case temporaryPasswordCreationFailed
}

public protocol SecureTransformRepositoryAPI {

    var temporaryPassword: AnyPublisher<Data, SecureTransformError> { get }
    
    func encryptWallet(
        _ wallet: Wallet,
        userId: String
    ) -> AnyPublisher<Data, SecureTransformError>
    
    func encryptWalletWithTemporaryPassword(
        _ wallet: Wallet,
        password: Data
    ) -> AnyPublisher<Data, SecureTransformError>
    
    func decryptWalletWithTemporaryPassword(
        _ encryptedData: Data,
        password: Data
    ) -> AnyPublisher<Wallet, SecureTransformError>
    
    func decryptWalletPayload(
        _ payload: Data,
        userId: String
    ) -> AnyPublisher<Wallet, SecureTransformError>
}
