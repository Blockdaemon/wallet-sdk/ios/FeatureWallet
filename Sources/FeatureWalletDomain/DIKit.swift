// Copyright © Blockdaemon All rights reserved.

import DIKit

extension DependencyContainer {
    
    // MARK: - FeatureWallet
    
    public static var featureWalletDomain = module {
        
        factory { WalletLifecycleService() as WalletLifecycleAPI }
        
        factory { CryptoAssetAddressService() as CryptoAssetAddressServiceAPI }
    }
}
