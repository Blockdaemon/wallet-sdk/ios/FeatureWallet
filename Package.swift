// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FeatureWallet",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "FeatureWallet",
            targets: ["FeatureWalletData", "FeatureWalletDomain"]
        ),
        .library(
            name: "FeatureWalletData",
            targets: ["FeatureWalletData"]
        ),
        .library(
            name: "FeatureWalletDomain",
            targets: ["FeatureWalletDomain"]
        )
    ],
    dependencies: [
        .package(
            url: "https://github.com/Liftric/DIKit.git",
            exact: "1.6.1"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/CommonCryptoKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Extensions",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ThresholdSecurity",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/NetworkKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ToolKit",
            exact: "1.0.0"
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "FeatureWalletDomain",
            dependencies: [
                "CommonCryptoKit",
                "Extensions",
                "ThresholdSecurity",
                "ToolKit",
                .product(name: "DIKit", package: "DIKit"),
            ]
        ),
        .target(
            name: "FeatureWalletData",
            dependencies: [
                .target(name: "FeatureWalletDomain"),
                "ToolKit",
                "Extensions",
                "ThresholdSecurity",
                "NetworkKit",
                .product(name: "DIKit", package: "DIKit"),
            ]
        )
    ]
)
